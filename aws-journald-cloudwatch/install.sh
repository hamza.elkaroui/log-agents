mkdir /tmp/logagent
cd /tmp/logagent
sudo apt install unzip
curl -OL  https://github.com/saymedia/journald-cloudwatch-logs/releases/download/v0.0.1/journald-cloudwatch-logs-linux.zip
unzip journald-cloudwatch-logs-linux.zip
rm journald-cloudwatch-logs-linux.zip
sudo mv journald-cloudwatch-logs/journald-cloudwatch-logs /usr/bin
sudo mkdir -p /var/lib/journald-cloudwatch-logs/
sudo mv journald-cloudwatch.conf /etc/
sudo mv journald.unit /etc/systemd/system/journald-cloudwatch.service
sudo chmod 664 /etc/systemd/system/journald-cloudwatch.service
sudo chown -R ubuntu /var/lib/journald-cloudwatch-logs/
sudo systemctl enable journald-cloudwatch.service
sudo systemctl start journald-cloudwatch.service
